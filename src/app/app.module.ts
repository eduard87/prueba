import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

//Rutas
import { AppRoutingModule } from './app.routes';

import { EditPostComponent } from './component/posts/edit-post/edit-post.component';
import { CreatePostComponent } from './component/posts/create-post/create-post.component';
import { GetPostComponent } from './component/posts/get-post/get-post.component';

@NgModule({
  declarations: [
    AppComponent,
    GetPostComponent,
    EditPostComponent,
    CreatePostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
