import { Routes, RouterModule } from '@angular/router';
import { NgModule, ModuleWithProviders } from '@angular/core';

import { GetPostComponent } from './component/posts/get-post/get-post.component';
import { EditPostComponent } from './component/posts/edit-post/edit-post.component';
import { CreatePostComponent } from './component/posts/create-post/create-post.component';

const APP_ROUTES: Routes = [
    { path: '', redirectTo: 'inicio', pathMatch: 'full' },     
    { path: '', component: GetPostComponent },
    { path: 'register', component: CreatePostComponent },
    { path: 'edit/:id', component: EditPostComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(APP_ROUTES)],
    exports: [RouterModule]
})
export class AppRoutingModule { }