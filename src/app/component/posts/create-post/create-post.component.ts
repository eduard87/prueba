import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceService } from '../../../service/service.service';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  constructor( private formB:FormBuilder, private serv:ServiceService ) { }

  formCreate:FormGroup;
  ngOnInit() {
    this.formCreate = this.formB.group({
      title:[null, [Validators.required,Validators.minLength(3)]],
      body:[null, [Validators.required,Validators.minLength(6)]]
    })
  }

  msgShow:boolean=false;
  create(e){
    this.msgShow=false;
    let title = this.formCreate.value.title;
    let body = this.formCreate.value.body;
    this.serv.createPost(title, body).subscribe( resp=>{
      this.msgShow=true;
      setTimeout( ()=>{
        this.msgShow=false;
      }, 10000);
    })
  }

}
