import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServiceService } from '../../../service/service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {

  constructor( 
      private route:ActivatedRoute, 
      private serv:ServiceService,
      private formB:FormBuilder
    ) { }

  id;
  formEdit:FormGroup;
  ngOnInit() {
    
    this.route.params.subscribe(params => {
      this.id = +params['id'];
    });
    this.getPost();
    
    this.formEdit=this.formB.group({
      title:[this.title,[Validators.required]],
      body:[this.body,[Validators.required]]
    })
  }

  title;
  body;
  getPost(){
    this.serv.putPostId( this.id ).subscribe( resp=>{
      console.log( resp );
      this.title=resp.title;
      this.body=resp.body
    });
    
  }

  msgShow:boolean=false;
  editPost(e){
    e.preventDefault();
    this.msgShow=false;
    let title = this.formEdit.value.title;
    let body = this.formEdit.value.body;
    this.serv.putPost(title, this.id, body).subscribe( resp=>{
      this.msgShow=true;
      setTimeout( ()=>{
        this.msgShow=false;
      }, 10000);
    })
  }


}
