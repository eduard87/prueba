import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../../service/service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-get-post',
  templateUrl: './get-post.component.html',
  styleUrls: ['./get-post.component.css']
})
export class GetPostComponent implements OnInit {

  constructor( private serv:ServiceService, private formB:FormBuilder ) { }

  form:FormGroup;
  ngOnInit() {
    this.form=this.formB.group({
      select:['']
    });
    this.getPosts();
  }

  lstPost:any;
  getPosts(){
    this.serv.getPost().subscribe( resp => {
      console.log(resp);
      this.lstPost=resp;
    });
  }

  msgShow:boolean=false;
  delete( id ){
    this.msgShow=false;
    this.serv.deletePost( id ).subscribe( resp=>{
      this.msgShow=true;
      this.getPosts();
      setTimeout( ()=>{
        this.msgShow=false;
      }, 10000);
    });
  }

  selectUser(e){
    if(e == 0){
      this.getPosts();
    }else{
      this.serv.getPostUser(e).subscribe( resp => {
        console.log(resp);
        this.lstPost=resp;
      });
    }
  }
  
  users=[
    {val:1,name:'user 1'},{val:2,name:'user 2'},{val:3,name:'user 3'},{val:4,name:'user 4'},
    {val:5,name:'user 5'},{val:6,name:'user 6'},{val:7,name:'user 7'},{val:8,name:'user 8'},
    {val:9,name:'user 9'},{val:10,name:'user 10'},
  ]

}
