import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor( private http:HttpClient ) { }
  
  getPost():Observable<any>{
    return this.http.get(`${ environment.url }/posts` );
  }

  getPostUser( idUser:number ):Observable<any>{
    return this.http.get(`${ environment.url }/posts?userdId?=${ idUser }`);
  }

  putPostId( idPost:number ):Observable<any>{
    return this.http.put(`${ environment.url }/posts/${ idPost }`,{});
  }

  createPost( title:string, body:string ):Observable<any>{
    return this.http.post(`${ environment.url }/posts`,{
      'title':title,
      'body':body
    });
  }

  putPost( title, idPost, body ):Observable<any>{
    return this.http.put(`${ environment.url }/posts/${ idPost }`,{
      'title':title,
      'body':body
    });
  }

  deletePost( idPost ):Observable<any>{
    return this.http.delete(`${ environment.url }/posts/${ idPost }`,{});
  }

}
